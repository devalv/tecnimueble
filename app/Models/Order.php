<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'order_date',
        'shipped_date',
        'status_id',
        'customer_id',
        'notes',
    
    ];
    
    
    protected $dates = [
        'order_date',
        'shipped_date',
        'created_at',
        'updated_at',
    
    ];

    protected $with = ['order_state', 'customer'];

    
    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/orders/'.$this->getKey());
    }

    public function order_state() {
        return $this->belongsTo(OrderState::class, 'status_id');
    }
    
    public function customer() {
        return $this->belongsTo(Customer::class, 'customer_id');
    }

    public function orderDetails() {
        return $this->hasMany(OrderDetails::class);
    }
}
