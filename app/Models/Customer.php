<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
        'id_num',
        'id_type',
        'name',
        'lastname',
        'email',
        'password',
        'enabled',
        'phone',
        'addressLine1',
        'addressLine2',
        'city',
        'country',
        'cart'
    ];
    
    protected $hidden = [
        'password',
    
    ];
    
    protected $dates = [
        'created_at',
        'updated_at',
    
    ];
    
    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/customers/'.$this->getKey());
    }
    public function orders() {
        return $this->hasMany(Order::class);
    }
    public function payments() {
        return $this->hasMany(Payment::class);
    }
}
