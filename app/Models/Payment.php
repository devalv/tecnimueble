<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'customer_id',
        'cardNumber',
        'cardMonth',
        'cardYear',
        'cardCVC',
        'amount',
        'paymentDate',
    
    ];
    
    
    protected $dates = [
        'paymentDate',
        'created_at',
        'updated_at',
    
    ];
    
    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/payments/'.$this->getKey());
    }

    public function customer() {
        return $this->belongsTo(Customer::class, 'customer_id');
    }
}
