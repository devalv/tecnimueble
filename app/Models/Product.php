<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Brackets\Media\HasMedia\ProcessMediaTrait;
use Brackets\Media\HasMedia\AutoProcessMediaTrait;
use Brackets\Media\HasMedia\HasMediaCollectionsTrait;
use Brackets\Media\HasMedia\HasMediaThumbsTrait;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;


class Product extends Model implements HasMedia {
    use ProcessMediaTrait;
    use AutoProcessMediaTrait;
    use HasMediaCollectionsTrait;
    use HasMediaThumbsTrait;


    protected $fillable = [
        'code',
        'name',
        'price',
        'cost',
        'category_id',
        'description',
        'stock',
        'origin',
        'is_featured'
    ];
    
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    protected $with = ['category'];
    
    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/products/'.$this->getKey());
    }
    
    public function category() {
        return $this->belongsTo(Category::class);
    }

    public function orderDetails() {
        return $this->hasMany(OrderDetails::class);
    }

    public function registerMediaCollections() : void
    {
        $this->addMediaCollection('images')->accepts('image/*')->maxNumberOfFiles(10);
    }
    public function registerMediaConversions(Media $media = null) : void
    {
        $this->autoRegisterThumb200();
    }

}
