<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $fillable = [
        'order_id',
        'product_id',
        'quantity',
        'price',
    
    ];
    
    
    protected $dates = [
        'created_at',
        'updated_at',
    
    ];
    
    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/order-details/'.$this->getKey());
    }
    
    public function order() {
        return $this->belongsTo(Order::class, 'order_id');
    }
    
    public function product() {
        return $this->belongsTo(Product::class, 'product_id');
    }
}
