<?php

namespace App\Http\Requests\Admin\OrderDetail;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateOrderDetail extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.order-detail.edit', $this->orderDetail);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'order' => ['required'],
            'product' => ['required'],
            'quantity' => ['sometimes', 'integer'],
            'price' => ['sometimes', 'numeric'],
            
        ];
    }

    /**
     * Modify input data
     *
     * @return array
     */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();


        //Add your code for manipulation with request data here

        return $sanitized;
    }
    public function getProductId(){
        if ($this->has('product')){
            return $this->get('product')['id'];
        }
        return null;
    }
    public function getOrderId(){
        if ($this->has('order')){
            return $this->get('order')['id'];
        }
        return null;
    }
}
