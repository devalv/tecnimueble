<?php

namespace App\Http\Requests\Admin\Customer;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateCustomer extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.customer.edit', $this->customer);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'id_num' => ['sometimes', Rule::unique('customers', 'id_num')->ignore($this->customer->getKey(), $this->customer->getKeyName()), 'string'],
            'id_type' => ['sometimes', 'string'],
            'name' => ['sometimes', 'string'],
            'lastname' => ['sometimes', 'string'],
            'email' => ['sometimes', 'email', Rule::unique('customers', 'email')->ignore($this->customer->getKey(), $this->customer->getKeyName()), 'string'],
            'password' => ['sometimes', 'confirmed', 'min:7', 'regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9]).*$/', 'string'],
            'enabled' => ['sometimes', 'boolean'],
            'phone' => ['sometimes', 'string'],
            'addressLine1' => ['sometimes', 'string'],
            'addressLine2' => ['sometimes', 'string'],
            'city' => ['sometimes', 'string'],
            'country' => ['sometimes', 'string'],
            
        ];
    }

    /**
     * Modify input data
     *
     * @return array
     */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();


        //Add your code for manipulation with request data here

        return $sanitized;
    }
}
