<?php

namespace App\Http\Requests\Admin\Payment;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdatePayment extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.payment.edit', $this->payment);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'customer_id' => ['required'],
            'cardNumber' => ['sometimes', 'string'],
            'cardMonth' => ['sometimes', 'string'],
            'cardYear' => ['sometimes', 'string'],
            'cardCVC' => ['sometimes', 'string'],
            'amount' => ['sometimes', 'numeric'],
            'paymentDate' => ['sometimes', 'date'],
            
        ];
    }

    /**
     * Modify input data
     *
     * @return array
     */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();


        //Add your code for manipulation with request data here

        return $sanitized;
    }
    public function getCustomerId(){
        if ($this->has('customer')){
            return $this->get('customer')['id'];
        }
        return null;
    }
}
