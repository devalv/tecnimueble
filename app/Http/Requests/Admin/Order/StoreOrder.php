<?php

namespace App\Http\Requests\Admin\Order;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StoreOrder extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('admin.order.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'order_date' => ['required', 'date'],
            'shipped_date' => ['nullable', 'date'],
            'order_state' => ['required'],
            'customer' => ['required'],
            'notes' => ['nullable', 'string'],
            
        ];
    }

    /**
    * Modify input data
    *
    * @return array
    */
    public function getSanitized(): array
    {
        $sanitized = $this->validated();

        //Add your code for manipulation with request data here

        return $sanitized;
    }

    public function getCustomerId() {
        if ($this->has('customer')){
            return $this->get('customer')['id'];
        }
        return null;
    }

    public function getOrderStateId() {
        if ($this->has('order_state')){
            return $this->get('order_state')['id'];
        }
        return null;
    }
}
