<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\OrderDetail\BulkDestroyOrderDetail;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\OrderDetail\DestroyOrderDetail;
use App\Http\Requests\Admin\OrderDetail\IndexOrderDetail;
use App\Http\Requests\Admin\OrderDetail\StoreOrderDetail;
use App\Http\Requests\Admin\OrderDetail\UpdateOrderDetail;
use App\Models\Product;
use App\Models\Order;
use App\Models\OrderDetail;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class OrderDetailsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexOrderDetail $request
     * @return array|Factory|View
     */
    public function index(IndexOrderDetail $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(OrderDetail::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'order_id', 'product_id', 'quantity', 'price'],

            // set columns to searchIn
            ['id'],

            function ($query) use ($request) {
                $query->with(['product', 'order']);
                if($request->has('products')){
                    $query->whereIn('product_id', $request->get('products'));
                }
                if($request->has('order')){
                    $query->whereIn('order_id', $request->get('order'));
                }
            }
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.order-detail.index', [
            'data' => $data,
            'orders' => Order::all(),
            'products' => Product::all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.order-detail.create');

        return view('admin.order-detail.create', [
            'products' => Product::all(),
            'orders' => Order::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreOrderDetail $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreOrderDetail $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();
        $sanitized['product_id'] = $request->getProductId();
        $sanitized['order_id'] = $request->getOrderId();

        // Store the OrderDetail
        $orderDetail = OrderDetail::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/order-details'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/order-details');
    }

    /**
     * Display the specified resource.
     *
     * @param OrderDetail $orderDetail
     * @throws AuthorizationException
     * @return void
     */
    public function show(OrderDetail $orderDetail)
    {
        $this->authorize('admin.order-detail.show', $orderDetail);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param OrderDetail $orderDetail
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(OrderDetail $orderDetail)
    {
        $this->authorize('admin.order-detail.edit', $orderDetail);


        return view('admin.order-detail.edit', [
            'orderDetail' => $orderDetail,
            'products' => Product::all(),
            'orders' => Order::all(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateOrderDetail $request
     * @param OrderDetail $orderDetail
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateOrderDetail $request, OrderDetail $orderDetail)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();
        $sanitized['product_id'] = $request->getProductId();
        $sanitized['order_id'] = $request->getOrderId();
        // Update changed values OrderDetail
        $orderDetail->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/order-details'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/order-details');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyOrderDetail $request
     * @param OrderDetail $orderDetail
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyOrderDetail $request, OrderDetail $orderDetail)
    {
        $orderDetail->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyOrderDetail $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyOrderDetail $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    OrderDetail::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
