<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\OrderState\BulkDestroyOrderState;
use App\Http\Requests\Admin\OrderState\DestroyOrderState;
use App\Http\Requests\Admin\OrderState\IndexOrderState;
use App\Http\Requests\Admin\OrderState\StoreOrderState;
use App\Http\Requests\Admin\OrderState\UpdateOrderState;
use App\Models\OrderState;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class OrderStatesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexOrderState $request
     * @return array|Factory|View
     */
    public function index(IndexOrderState $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(OrderState::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'name'],

            // set columns to searchIn
            ['id', 'name']
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }

        return view('admin.order-state.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.order-state.create');

        return view('admin.order-state.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreOrderState $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreOrderState $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Store the OrderState
        $orderState = OrderState::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/order-states'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/order-states');
    }

    /**
     * Display the specified resource.
     *
     * @param OrderState $orderState
     * @throws AuthorizationException
     * @return void
     */
    public function show(OrderState $orderState)
    {
        $this->authorize('admin.order-state.show', $orderState);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param OrderState $orderState
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(OrderState $orderState)
    {
        $this->authorize('admin.order-state.edit', $orderState);


        return view('admin.order-state.edit', [
            'orderState' => $orderState,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateOrderState $request
     * @param OrderState $orderState
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateOrderState $request, OrderState $orderState)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();

        // Update changed values OrderState
        $orderState->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/order-states'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/order-states');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyOrderState $request
     * @param OrderState $orderState
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyOrderState $request, OrderState $orderState)
    {
        $orderState->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyOrderState $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyOrderState $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    OrderState::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }
}
