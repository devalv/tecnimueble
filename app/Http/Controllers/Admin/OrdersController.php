<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Order\BulkDestroyOrder;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\Order\DestroyOrder;
use App\Http\Requests\Admin\Order\IndexOrder;
use App\Http\Requests\Admin\Order\StoreOrder;
use App\Http\Requests\Admin\Order\UpdateOrder;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Product;
use App\Models\Customer;
use App\Models\OrderState;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Illuminate\Support\Facades\Mail;
use App\Mail\NewOrder;

class OrdersController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexOrder $request
     * @return array|Factory|View
     */
    public function index(IndexOrder $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Order::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'order_date', 'shipped_date', 'status_id', 'customer_id', 'notes'],

            // set columns to searchIn
            ['id', 'notes'],

            function ($query) use ($request) {
                $query->with(['order_state', 'customer']);
                if($request->has('order_states')){
                    $query->whereIn('status_id', $request->get('order_states'));
                }
                if($request->has('customers')){
                    $query->whereIn('customer_id', $request->get('customers'));
                }
            }
        );

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return ['data' => $data];
        }
        return view('admin.order.index', [
            'data' => $data, 
            'customers' => Customer::all(), 
            'order_states' => OrderState::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.order.create');

        return view('admin.order.create', [
            'customers' => Customer::all(), 
            'order_states' => OrderState::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreOrder $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreOrder $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();
        $sanitized['status_id'] = $request->getOrderStateId();
        $sanitized['customer_id'] = $request->getCustomerId();

        // Store the Order
        $order = Order::create($sanitized);
        
        if ($request->ajax()) {
            return ['redirect' => url('admin/orders'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/orders');
    }

    /**
     * Display the specified resource.
     *
     * @param Order $order
     * @throws AuthorizationException
     * @return void
     */
    public function show(Order $order)
    {
        $this->authorize('admin.order.show', $order);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Order $order
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(Order $order)
    {
        $this->authorize('admin.order.edit', $order);


        return view('admin.order.edit', [
            'order' => $order,
            'customers' => Customer::all(), 
            'order_states' => OrderState::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateOrder $request
     * @param Order $order
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateOrder $request, Order $order)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();
        $sanitized['status_id'] = $request->getOrderStateId();
        $sanitized['customer_id'] = $request->getCustomerId();

        // Update changed values Order
        $order->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/orders'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/orders');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyOrder $request
     * @param Order $order
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyOrder $request, Order $order)
    {
        $order->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyOrder $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyOrder $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    Order::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }

    public function getAllOrders() {
        return response()->json(Order::all(), 200);
    }

    public function getOrderById(Request $request, $id, $phpObject = false) {
        try{
            $order = Order::findOrFail($id);
            $details = OrderDetail::where('order_id',$order['id'])->get();
            $detailsWithMedia = [];
            for ($i = 0; $i < count($details); $i += 1) {
                $detail = $details[$i];
                $product_id = $detail['product_id'];
                $product = Product::find($product_id);
                $first_image = $product->getMedia('images')->first();
                if ($first_image == null) {
                    $empty_array = (array) null;
                    $placeholder_array = array(
                        'id' => 0,
                        'name' =>'placeholder',
                        'file_name' => 'placeholder.jpg'
                    );
                    $product->media = [$placeholder_array];
                } else {
                    $media = $first_image->getUrl();
                    $product->media = $media;
                }
                $detail['product'] = $product;
                array_push($detailsWithMedia, $detail);
            }

            $order['details'] = $detailsWithMedia;
            if ($phpObject == true) {
                return json_decode($order, true);
            }
            return response()->json($order, 200);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            $errorResJson = $e
                ->getResponse()
                ->getBody()
                ->getContents();
            $errorRes = json_decode(stripslashes($errorResJson), true);
            // Return error
            return response()->json(
                [
                'message' => 'error',
                'data' => $errorRes
                ],
                $errorRes['response']['code']
            );
        }
    }
    
    public function getOrderByCustomerId(Request $request, $id) {
        try {
            $orders = Order::where('customer_id',$id)->get();
            $ordersWithDetails = [];
            for ($i = 0; $i < count($orders); $i += 1) {
                $order = $orders[$i];
                $details = OrderDetail::where('order_id',$order['id'])->get();
                $detailsWithMedia = [];
                for ($j = 0; $j < count($details); $j += 1) {
                    $detail = $details[$j];
                    $product_id = $detail['product_id'];
                    $product = Product::find($product_id);
                    $first_image = $product->getMedia('images')->first();
                    if ($first_image == null) {
                        $empty_array = (array) null;
                        $placeholder_array = array(
                            'id' => 0,
                            'name' =>'placeholder',
                            'file_name' => 'placeholder.jpg'
                        );
                        $product->media = [$placeholder_array];
                    } else {
                        $media = $first_image->getUrl();
                        $product->media = $media;
                    }
                    $detail['product'] = $product;
                    array_push($detailsWithMedia, $detail);
                }

                $order['details'] = $detailsWithMedia;
                array_push($ordersWithDetails, $order);
            }
            return response()->json($ordersWithDetails, 200);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            $errorResJson = $e
                ->getResponse()
                ->getBody()
                ->getContents();
            $errorRes = json_decode(stripslashes($errorResJson), true);
            // Return error
            return response()->json(
                [
                'message' => 'error',
                'data' => $errorRes
                ],
                $errorRes['response']['code']
            );
        }
    }
    
    public function placeOrder(Request $request, $customer_id) {
        try {
            $body = json_decode($request->getContent(), true);
            $cart = $body['cart'];
            $notes = $body['notes'];
            $status_id = 1; // Generada
            $order_date = date('Y-m-d');
            $order_id = Order::insertGetId([
                'status_id' => $status_id,
                'customer_id' => $customer_id,
                'order_date' => $order_date,
                'notes' => $notes,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s'),
            ]);
            $order_details = [];
            for ($i = 0; $i < count($cart); $i += 1) {
                $item = $cart[$i];
                $product_id = $item['product'];
                $quantity = $item['qty'];
                $price = $item['price'];
                $order_detail = [
                    'order_id' => $order_id,
                    'product_id' => $product_id,
                    'quantity' => $quantity,
                    'price' => $price,
                    'created_at' => date('Y-m-d h:i:s'),
                    'updated_at' => date('Y-m-d h:i:s')
                ];
                $current_product = Product::find($product_id);
                $current_product->stock = $current_product['stock'] - $quantity;
                $current_product->save();
                array_push($order_details, $order_detail);
            }
            OrderDetail::insert($order_details);
            $current_customer = Customer::find($customer_id);
            $current_customer->cart = null;
            $current_customer->save();
            $mailerObject = $this->getOrderById(new Request(), $order_id, true);
            Mail::to($current_customer['email'])->send(new NewOrder($mailerObject));
            Mail::to('ventas@tecnimueble.com.ec')->send(new NewOrder($mailerObject));
            return response()->json(['detail'=>$order_details, 'id' => $order_id, 'customer' => $current_customer], 200);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            $errorResJson = $e
                ->getResponse()
                ->getBody()
                ->getContents();
            $errorRes = json_decode(stripslashes($errorResJson), true);
            // Return error
            return response()->json(
                [
                'message' => 'error',
                'data' => $errorRes
                ],
                $errorRes['response']['code']
            );
        }
    }
}
