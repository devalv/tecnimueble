<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\Product\BulkDestroyProduct;
use App\Http\Requests\Admin\Product\DestroyProduct;
use App\Http\Requests\Admin\Product\IndexProduct;
use App\Http\Requests\Admin\Product\StoreProduct;
use App\Http\Requests\Admin\Product\UpdateProduct;
use App\Models\Product;
use App\Models\Category;
use Brackets\AdminListing\Facades\AdminListing;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class ProductsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexProduct $request
     * @return array|Factory|View
     */
    public function index(IndexProduct $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Product::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'code', 'name', 'price', 'cost', 'category_id', 'description', 'stock', 'origin','is_featured'],

            // set columns to searchIn
            ['id', 'code', 'name', 'description', 'origin'],

            function ($query) use ($request) {
                $query->with(['category']);
                if ($request->has('categories')) {
                    $query->whereIn('category_id', $request->get('categories'));
                }
            }
        );

        $products = $data;
        $productsWithMedia = [];
        
        for ($i = 0; $i < count($products); $i += 1) {
            $product = $products[$i];
            $first_image = $product->getMedia('images')->first();
            if ($first_image == null) {
                $empty_array = (array) null;
                $placeholder_array = array(
                    'id' => 0,
                    'name' =>'placeholder',
                    'file_name' => 'placeholder.jpg'
                );
                $product->media = [$placeholder_array];
                array_push($productsWithMedia, $product);

            } else {
                $media = $first_image->getUrl();
                $product->media = $media;
                array_push($productsWithMedia, $product);
            }
        }

        if ($request->ajax()) {
            if ($request->has('bulk')) {
                return [
                    'bulkItems' => $data->pluck('id')
                ];
            }
            return [
                'data' => $data
            ];
        }
        $data->data = $productsWithMedia;
        return view('admin.product.index', [
            'data' => $data,
            'categories' => Category::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function create()
    {
        $this->authorize('admin.product.create');

        return view('admin.product.create',[
            'categories' => Category::all(),
            'mode' => 'create',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreProduct $request
     * @return array|RedirectResponse|Redirector
     */
    public function store(StoreProduct $request)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();
        $sanitized['category_id'] = $request->getCategoryId();

        // Store the Product
        $product = Product::create($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/products'), 
                'message' => trans('brackets/admin-ui::admin.operation.succeeded')
            ];
        }

        return redirect('admin/products');
    }

    /**
     * Display the specified resource.
     *
     * @param Product $product
     * @throws AuthorizationException
     * @return void
     */
    public function show(Product $product)
    {
        $this->authorize('admin.product.show', $product);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Product $product
     * @throws AuthorizationException
     * @return Factory|View
     */
    public function edit(Product $product)
    {
        $this->authorize('admin.product.edit', $product);


        return view('admin.product.edit', [
            'product' => $product,
            'categories' => Category::all(),
            'mode' => 'edit'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateProduct $request
     * @param Product $product
     * @return array|RedirectResponse|Redirector
     */
    public function update(UpdateProduct $request, Product $product)
    {
        // Sanitize input
        $sanitized = $request->getSanitized();
        $sanitized['category_id'] = $request->getCategoryId();

        // Update changed values Product
        $product->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('admin/products'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded'),
            ];
        }

        return redirect('admin/products');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyProduct $request
     * @param Product $product
     * @throws Exception
     * @return ResponseFactory|RedirectResponse|Response
     */
    public function destroy(DestroyProduct $request, Product $product)
    {
        $product->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param BulkDestroyProduct $request
     * @throws Exception
     * @return Response|bool
     */
    public function bulkDestroy(BulkDestroyProduct $request) : Response
    {
        DB::transaction(static function () use ($request) {
            collect($request->data['ids'])
                ->chunk(1000)
                ->each(static function ($bulkChunk) {
                    Product::whereIn('id', $bulkChunk)->delete();

                    // TODO your code goes here
                });
        });

        return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
    }

    public function getAllProducts(Request $request) {
        $products = Product::get();
        $productsWithMedia = [];

        for ($i = 0; $i < count($products); $i += 1) {
            $product = $products[$i];
            $first_image = $product->getMedia('images')->first();
            if ($first_image == null) {
                $empty_array = (array) null;
                $placeholder_array = array(
                    'id' => 0,
                    'name' =>'placeholder',
                    'file_name' => 'placeholder.jpg'
                );
                $product->media = [$placeholder_array];
                array_push($productsWithMedia, $product);

            } else {
                $media = $first_image->getUrl();
                $product->media = $media;
                array_push($productsWithMedia, $product);
            }
        }


        return response()->json($productsWithMedia, 200);
    }

    public function getFeaturedProducts() {
        $products = Product::where('is_featured', true)->get();
        $productsWithMedia = [];

        for ($i = 0; $i < count($products); $i += 1) {
            $product = $products[$i];
            $first_image = $product->getMedia('images')->first();
            if ($first_image == null) {
                $empty_array = (array) null;
                $placeholder_array = array(
                    'id' => 0,
                    'name' =>'placeholder',
                    'file_name' => 'placeholder.jpg'
                );
                $product->media = [$placeholder_array];
                array_push($productsWithMedia, $product);

            } else {
                $media = $first_image->getUrl();
                $product->media = $media;
                array_push($productsWithMedia, $product);
            }
        }


        return response()->json($productsWithMedia, 200);
    }

    public function getProductById(Request $request, $id) {
        $product = Product::findOrFail($id);
        $first_image = $product->getMedia('images')->first();
        if ($first_image == null) {
            $empty_array = (array) null;
            $placeholder_array = array(
                'id' => 0,
                'name' =>'placeholder',
                'file_name' => 'placeholder.jpg'
            );
            $product->media = [$placeholder_array];
        } else {
            $media = $first_image->getUrl();
            $product->media = $media;
        }

        return response()->json($product, 200);
    }

    public function getProductsByCategory(Request $request, $category_id) {
        $products = Product::where('category_id', $category_id)->get();
        $productsWithMedia = [];

        for ($i = 0; $i < count($products); $i += 1) {
            $product = $products[$i];
            $first_image = $product->getMedia('images')->first();
            if ($first_image == null) {
                $empty_array = (array) null;
                $placeholder_array = array(
                    'id' => 0,
                    'name' =>'placeholder',
                    'file_name' => 'placeholder.jpg'
                );
                $product->media = [$placeholder_array];
                array_push($productsWithMedia, $product);

            } else {
                $media = $first_image->getUrl();
                $product->media = $media;
                array_push($productsWithMedia, $product);
            }
        }


        return response()->json($productsWithMedia, 200);
    }

    public function getProducts(Request $request) {
        try {
            $body = json_decode($request->getContent(), true);
            if ($body['categoryId'] && $body['search'] && $body['orderBy'] && $body['orderDirection']) {
                $searchText = '%'.$body['search'].'%';
                $products = Product::where([['name','LIKE', $searchText], ['category_id', $body['categoryId']]])->orderBy($body['orderBy'], $body['orderDirection'])->get();
            

            } else if ($body['search'] && $body['orderBy'] && $body['orderDirection']) {
                $searchText = '%'.$body['search'].'%';
                $products = Product::where('name','LIKE',$searchText)->orderBy($body['orderBy'], $body['orderDirection'])->get();
            

            } else if ($body['categoryId'] && $body['orderBy'] && $body['orderDirection']) {
            
                $products = Product::where('category_id', $body['categoryId'])->orderBy($body['orderBy'], $body['orderDirection'])->get();
            

            } else if ($body['orderBy'] && $body['orderDirection']) {
            
                $products = Product::orderBy($body['orderBy'], $body['orderDirection'])->get();
            

            } else {
            
                $products = Product::get();
            
            }
            $productsWithMedia = [];

            for ($i = 0; $i < count($products); $i += 1) {
                $product = $products[$i];
                $first_image = $product->getMedia('images')->first();
                if ($first_image == null) {
                    $empty_array = (array) null;
                    $placeholder_array = array(
                        'id' => 0,
                        'name' =>'placeholder',
                        'file_name' => 'placeholder.jpg'
                    );
                    $product->media = [$placeholder_array];
                    array_push($productsWithMedia, $product);

                } else {
                    $media = $first_image->getUrl();
                    $product->media = $media;
                    array_push($productsWithMedia, $product);
                }
            }

            
            return response()->json(['results' => count($products), 'products' => $products],200);

        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            $errorResJson = $e
                ->getResponse()
                ->getBody()
                ->getContents();
            $errorRes = json_decode(stripslashes($errorResJson), true);
            // Return error
            return response()->json(
                [
                'message' => 'error',
                'data' => $errorRes
                ],
                $errorRes['response']['code']
            );
        }


        return response()->json($productsWithMedia, 200);
    }
}
