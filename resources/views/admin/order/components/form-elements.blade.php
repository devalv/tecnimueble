<div class="form-group row align-items-center" :class="{'has-danger': errors.has('order_date'), 'has-success': fields.order_date && fields.order_date.valid }">
    <label for="order_date" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.order.columns.order_date') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-sm-8'">
        <div class="input-group input-group--custom">
            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            <datetime v-model="form.order_date" :config="datePickerConfig" v-validate="'required|date_format:yyyy-MM-dd HH:mm:ss'" class="flatpickr" :class="{'form-control-danger': errors.has('order_date'), 'form-control-success': fields.order_date && fields.order_date.valid}" id="order_date" name="order_date" placeholder="{{ trans('brackets/admin-ui::admin.forms.select_a_date') }}"></datetime>
        </div>
        <div v-if="errors.has('order_date')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('order_date') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('shipped_date'), 'has-success': fields.shipped_date && fields.shipped_date.valid }">
    <label for="shipped_date" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.order.columns.shipped_date') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-sm-8'">
        <div class="input-group input-group--custom">
            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            <datetime v-model="form.shipped_date" :config="datePickerConfig" v-validate="'date_format:yyyy-MM-dd HH:mm:ss'" class="flatpickr" :class="{'form-control-danger': errors.has('shipped_date'), 'form-control-success': fields.shipped_date && fields.shipped_date.valid}" id="shipped_date" name="shipped_date" placeholder="{{ trans('brackets/admin-ui::admin.forms.select_a_date') }}"></datetime>
        </div>
        <div v-if="errors.has('shipped_date')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('shipped_date') }}</div>
    </div>
</div>
<div 
    class="form-group row align-items-center" 
    :class="{'has-danger': errors.has('status_id'), 'has-success': fields.status_id && fields.status_id.valid }"
>
    <label for="status_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.product.columns.status_id') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <multiselect
            v-model="form.order_state"
            :options="order_states"
            :multiple="false"
            track-by="id"
            label="name"
            tag-placeholder="{{ __('Select Order State') }}"
            placeholder="{{ __('Order State') }}">
        </multiselect>
        <div v-if="errors.has('status_id')" class="form-control-feedback form-text" v-cloak>@{{errors.first('status_id') }}</div>
    </div>
</div>

<div 
    class="form-group row align-items-center" 
    :class="{'has-danger': errors.has('customer_id'), 'has-success': fields.customer_id && fields.customer_id.valid }"
>
    <label for="customer_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.product.columns.customer_id') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <multiselect
            v-model="form.customer"
            :options="customers"
            :multiple="false"
            track-by="id"
            label="id_num"
            tag-placeholder="{{ __('Select Customer') }}"
            placeholder="{{ __('Customer') }}">
        </multiselect>
        <div v-if="errors.has('customer_id')" class="form-control-feedback form-text" v-cloak>@{{errors.first('customer_id') }}</div>
    </div>
</div>


<div class="form-group row align-items-center" :class="{'has-danger': errors.has('notes'), 'has-success': fields.notes && fields.notes.valid }">
    <label for="notes" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.order.columns.notes') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.notes" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('notes'), 'form-control-success': fields.notes && fields.notes.valid}" id="notes" name="notes" placeholder="{{ trans('admin.order.columns.notes') }}">
        <div v-if="errors.has('notes')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('notes') }}</div>
    </div>
</div>


