@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.order.actions.index'))

@section('body')

    <orders-listing
        :data="{{ $data->toJson() }}"
        :url="'{{ url('admin/orders') }}'"
        inline-template>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> {{ trans('admin.order.actions.index') }}
                        <a class="btn btn-primary btn-spinner btn-sm pull-right m-b-0" href="{{ url('admin/orders/create') }}" role="button"><i class="fa fa-plus"></i>&nbsp; {{ trans('admin.order.actions.create') }}</a>
                    </div>
                    <div class="card-body" v-cloak>
                        <div class="card-block">
                            <form @submit.prevent="">
                                <div class="row justify-content-md-between">
                                    <div class="col col-lg-7 col-xl-5 form-group">
                                        <div class="input-group">
                                            <input class="form-control" placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search') }}" v-model="search" @keyup.enter="filter('search', $event.target.value)" />
                                            <span class="input-group-append">
                                                <button type="button" class="btn btn-primary" @click="filter('search', search)"><i class="fa fa-search"></i>&nbsp; {{ trans('brackets/admin-ui::admin.btn.search') }}</button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col form-group deadline-checkbox-col">
                                        <div class="switch-filter-wrap">
                                            <label class="switch switch-3d switch-primary">
                                                <input type="checkbox" class="switch-input" v-model="showCustomersFilter" >
                                                <span class="switch-slider"></span>
                                            </label>
                                            <span 
                                                class="customers-filter" 
                                                style="position: absolute; margin-left: 5px; margin-top: 2px;"
                                            >
                                                &nbsp;{{ __('Customers filter') }}
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col form-group deadline-checkbox-col">
                                        <div class="switch-filter-wrap">
                                            <label class="switch switch-3d switch-primary">
                                                <input type="checkbox" class="switch-input" v-model="showOrderStatesFilter" >
                                                <span class="switch-slider"></span>
                                            </label>
                                            <span 
                                                class="order-states-filter" 
                                                style="position: absolute; margin-left: 5px; margin-top: 2px;"
                                            >
                                                &nbsp;{{ __('Order states filter') }}
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-sm-auto form-group ">
                                        <select class="form-control" v-model="pagination.state.per_page">
                                            
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="100">100</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row" v-if="showCustomersFilter">
                                    <div class="col-sm-auto form-group" style="margin-bottom: 0;">
                                        <p style="line-height: 40px; margin:0;">{{ __('Select customer/s') }}</p>
                                    </div>
                                    <div class="col col-lg-12 col-xl-12 form-group" style="max-width: 590px; ">
                                        <multiselect v-model="customersMultiselect"
                                                    :options="{{ $customers->map(function($customer) { return ['key' => $customer->id, 'label' =>  $customer->name.' '.$customer->lastname]; })->toJson() }}"
                                                    label="label"
                                                    track-by="key"
                                                    placeholder="{{ __('Type to search a customer/s') }}"
                                                    :limit="2"
                                                    :multiple="true">
                                        </multiselect>
                                    </div>
                                </div>
                                <div class="row" v-if="showOrderStatesFilter">
                                    <div class="col-sm-auto form-group">
                                        {{ __('Select order state/s') }}
                                    </div>
                                    <div class="col col-lg-12 col-xl-12 form-group" style="max-width: 590px; ">
                                        <multiselect 
                                            v-model="orderStatesMultiselect"
                                            :options="{{ $order_states->map(function($order_state) { return ['key' => $order_state->id, 'label' =>  $order_state->name]; })->toJson() }}"
                                            label="label"
                                            track-by="key"
                                            placeholder="{{ __('Type to search a order state') }}"
                                            :limit="2"
                                            :multiple="true"
                                        >
                                        </multiselect>
                                    </div>
                                </div>
                            </form>

                            <table class="table table-hover table-listing">
                                <thead>
                                    <tr>
                                        <th class="bulk-checkbox">
                                            <input class="form-check-input" id="enabled" type="checkbox" v-model="isClickedAll" v-validate="''" data-vv-name="enabled"  name="enabled_fake_element" @click="onBulkItemsClickedAllWithPagination()">
                                            <label class="form-check-label" for="enabled">
                                                #
                                            </label>
                                        </th>

                                        <th is='sortable' :column="'id'">{{ trans('admin.order.columns.id') }}</th>
                                        <th is='sortable' :column="'order_date'">{{ trans('admin.order.columns.order_date') }}</th>
                                        <th is='sortable' :column="'shipped_date'">{{ trans('admin.order.columns.shipped_date') }}</th>
                                        <th is='sortable' :column="'status_id'">{{ trans('admin.order.columns.status_id') }}</th>
                                        <th is='sortable' :column="'customer_id'">{{ trans('admin.order.columns.customer_id') }}</th>
                                        <th is='sortable' :column="'notes'">{{ trans('admin.order.columns.notes') }}</th>

                                        <th></th>
                                    </tr>
                                    <tr v-show="(clickedBulkItemsCount > 0) || isClickedAll">
                                        <td class="bg-bulk-info d-table-cell text-center" colspan="8">
                                            <span class="align-middle font-weight-light text-dark">{{ trans('brackets/admin-ui::admin.listing.selected_items') }} @{{ clickedBulkItemsCount }}.  <a href="#" class="text-primary" @click="onBulkItemsClickedAll('/admin/orders')" v-if="(clickedBulkItemsCount < pagination.state.total)"> <i class="fa" :class="bulkCheckingAllLoader ? 'fa-spinner' : ''"></i> {{ trans('brackets/admin-ui::admin.listing.check_all_items') }} @{{ pagination.state.total }}</a> <span class="text-primary">|</span> <a
                                                        href="#" class="text-primary" @click="onBulkItemsClickedAllUncheck()">{{ trans('brackets/admin-ui::admin.listing.uncheck_all_items') }}</a>  </span>

                                            <span class="pull-right pr-2">
                                                <button class="btn btn-sm btn-danger pr-3 pl-3" @click="bulkDelete('/admin/orders/bulk-destroy')">{{ trans('brackets/admin-ui::admin.btn.delete') }}</button>
                                            </span>

                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(item, index) in collection" :key="item.id" :class="bulkItems[item.id] ? 'bg-bulk' : ''">
                                        <td class="bulk-checkbox">
                                            <input class="form-check-input" :id="'enabled' + item.id" type="checkbox" v-model="bulkItems[item.id]" v-validate="''" :data-vv-name="'enabled' + item.id"  :name="'enabled' + item.id + '_fake_element'" @click="onBulkItemClicked(item.id)" :disabled="bulkCheckingAllLoader">
                                            <label class="form-check-label" :for="'enabled' + item.id">
                                            </label>
                                        </td>

                                    <td>@{{ item.id }}</td>
                                        <td>@{{ item.order_date | date }}</td>
                                        <td>@{{ item.shipped_date | date }}</td>
                                        <td><a :href="'/admin/order-states/'+item.order_state.id+'/edit'">@{{ item.order_state.name }}</a></td>
                                        <td><a :href="'/admin/customers/'+item.customer.id+'/edit'">@{{ item.customer.name+' '+item.customer.lastname }}</a></td>
                                        <td>@{{ item.notes }}</td>
                                        
                                        <td>
                                            <div class="row no-gutters">
                                                <div class="col-auto">
                                                    <a class="btn btn-sm btn-spinner btn-info" :href="item.resource_url + '/edit'" title="{{ trans('brackets/admin-ui::admin.btn.edit') }}" role="button"><i class="fa fa-edit"></i></a>
                                                </div>
                                                <form class="col" @submit.prevent="deleteItem(item.resource_url)">
                                                    <button type="submit" class="btn btn-sm btn-danger" title="{{ trans('brackets/admin-ui::admin.btn.delete') }}"><i class="fa fa-trash-o"></i></button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <div class="row" v-if="pagination.state.total > 0">
                                <div class="col-sm">
                                    <span class="pagination-caption">{{ trans('brackets/admin-ui::admin.pagination.overview') }}</span>
                                </div>
                                <div class="col-sm-auto">
                                    <pagination></pagination>
                                </div>
                            </div>

                            <div class="no-items-found" v-if="!collection.length > 0">
                                <i class="icon-magnifier"></i>
                                <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                                <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                                <a class="btn btn-primary btn-spinner" href="{{ url('admin/orders/create') }}" role="button"><i class="fa fa-plus"></i>&nbsp; {{ trans('admin.order.actions.create') }}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </orders-listing>

@endsection