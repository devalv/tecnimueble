@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.order-detail.actions.edit', ['name' => $orderDetail->id]))

@section('body')

    <div class="container-xl">
        <div class="card">

            <order-detail-form
                :action="'{{ $orderDetail->resource_url }}'"
                :data="{{ $orderDetail->toJson() }}"
                :orders="{{$orders->toJson()}}"
                :products="{{$products->toJson()}}"
                v-cloak
                inline-template
            >
            
                <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" :action="action" novalidate>


                    <div class="card-header">
                        <i class="fa fa-pencil"></i> {{ trans('admin.order-detail.actions.edit', ['name' => $orderDetail->id]) }}
                    </div>

                    <div class="card-body">
                        @include('admin.order-detail.components.form-elements')
                    </div>
                    
                    
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>
                    
                </form>

        </order-detail-form>

        </div>
    
</div>

@endsection