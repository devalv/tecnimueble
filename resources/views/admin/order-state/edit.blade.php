@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.order-state.actions.edit', ['name' => $orderState->name]))

@section('body')

    <div class="container-xl">
        <div class="card">

            <order-state-form
                :action="'{{ $orderState->resource_url }}'"
                :data="{{ $orderState->toJson() }}"
                v-cloak
                inline-template>
            
                <form class="form-horizontal form-edit" method="post" @submit.prevent="onSubmit" :action="action" novalidate>


                    <div class="card-header">
                        <i class="fa fa-pencil"></i> {{ trans('admin.order-state.actions.edit', ['name' => $orderState->name]) }}
                    </div>

                    <div class="card-body">
                        @include('admin.order-state.components.form-elements')
                    </div>
                    
                    
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>
                    
                </form>

        </order-state-form>

        </div>
    
</div>

@endsection