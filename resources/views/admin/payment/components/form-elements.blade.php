<div 
    class="form-group row align-items-center" 
    :class="{'has-danger': errors.has('customer_id'), 'has-success': fields.customer_id && fields.customer_id.valid }"
>
    <label for="customer_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.product.columns.customer_id') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <multiselect
            v-model="form.customer"
            :options="customers"
            :multiple="false"
            track-by="id"
            label="id_num"
            tag-placeholder="{{ __('Select Customer') }}"
            placeholder="{{ __('Customer') }}">
        </multiselect>
        <div v-if="errors.has('customer_id')" class="form-control-feedback form-text" v-cloak>@{{errors.first('customer_id') }}</div>
    </div>
</div>


<div class="form-group row align-items-center" :class="{'has-danger': errors.has('cardNumber'), 'has-success': fields.cardNumber && fields.cardNumber.valid }">
    <label for="cardNumber" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.payment.columns.cardNumber') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.cardNumber" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('cardNumber'), 'form-control-success': fields.cardNumber && fields.cardNumber.valid}" id="cardNumber" name="cardNumber" placeholder="{{ trans('admin.payment.columns.cardNumber') }}">
        <div v-if="errors.has('cardNumber')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('cardNumber') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('cardMonth'), 'has-success': fields.cardMonth && fields.cardMonth.valid }">
    <label for="cardMonth" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.payment.columns.cardMonth') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.cardMonth" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('cardMonth'), 'form-control-success': fields.cardMonth && fields.cardMonth.valid}" id="cardMonth" name="cardMonth" placeholder="{{ trans('admin.payment.columns.cardMonth') }}">
        <div v-if="errors.has('cardMonth')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('cardMonth') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('cardYear'), 'has-success': fields.cardYear && fields.cardYear.valid }">
    <label for="cardYear" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.payment.columns.cardYear') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.cardYear" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('cardYear'), 'form-control-success': fields.cardYear && fields.cardYear.valid}" id="cardYear" name="cardYear" placeholder="{{ trans('admin.payment.columns.cardYear') }}">
        <div v-if="errors.has('cardYear')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('cardYear') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('cardCVC'), 'has-success': fields.cardCVC && fields.cardCVC.valid }">
    <label for="cardCVC" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.payment.columns.cardCVC') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.cardCVC" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('cardCVC'), 'form-control-success': fields.cardCVC && fields.cardCVC.valid}" id="cardCVC" name="cardCVC" placeholder="{{ trans('admin.payment.columns.cardCVC') }}">
        <div v-if="errors.has('cardCVC')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('cardCVC') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('amount'), 'has-success': fields.amount && fields.amount.valid }">
    <label for="amount" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.payment.columns.amount') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.amount" v-validate="'required|decimal'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('amount'), 'form-control-success': fields.amount && fields.amount.valid}" id="amount" name="amount" placeholder="{{ trans('admin.payment.columns.amount') }}">
        <div v-if="errors.has('amount')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('amount') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('paymentDate'), 'has-success': fields.paymentDate && fields.paymentDate.valid }">
    <label for="paymentDate" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.payment.columns.paymentDate') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-sm-8'">
        <div class="input-group input-group--custom">
            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            <datetime v-model="form.paymentDate" :config="datePickerConfig" v-validate="'required|date_format:yyyy-MM-dd HH:mm:ss'" class="flatpickr" :class="{'form-control-danger': errors.has('paymentDate'), 'form-control-success': fields.paymentDate && fields.paymentDate.valid}" id="paymentDate" name="paymentDate" placeholder="{{ trans('brackets/admin-ui::admin.forms.select_a_date') }}"></datetime>
        </div>
        <div v-if="errors.has('paymentDate')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('paymentDate') }}</div>
    </div>
</div>


