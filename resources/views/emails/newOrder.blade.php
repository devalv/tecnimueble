<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="icon" href="assets/images/favicon/1.png" type="image/x-icon">
        <link rel="shortcut icon" href="assets/images/favicon/1.png" type="image/x-icon">
        <title>Tecnimueble </title>
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <style type="text/css">
            body{
            	text-align: center;
            	margin: 0 auto;
            	width: 650px;
            	font-family: 'Open Sans', sans-serif;
            	background-color: #e2e2e2;		      	
            	display: block;
                font-family: 'Roboto', sans-serif;
            }
            ul{
            	margin:0;
            	padding: 0;
            }
            li{
            	display: inline-block;
            	text-decoration: unset;
            }
            a{
            	text-decoration: none;
            }
            p{
                margin: 15px 0;
            }

            h5{
            	color:#444;
                text-align:left;
                font-weight:400;
            }
            
            .text-center{
            	text-align: center
            }
            .main-bg-light{
            	background-color: #fafafa;
            }
            .title{
            	color: #444444;
            	font-size: 22px;
            	font-weight: bold;
            	margin-top: 10px;
            	margin-bottom: 10px;
            	padding-bottom: 0;
            	text-transform: uppercase;
            	display: inline-block;
            	line-height: 1;
            }
            table{
                margin-top:30px
            }
            table.top-0{
                margin-top:0;
            }
            table.order-detail {
                border: 1px solid #ddd;
                border-collapse: collapse;
            }
            table.order-detail tr:nth-child(even) {
              border-top:1px solid #ddd;
              border-bottom:1px solid #ddd;
            }
            table.order-detail tr:nth-child(odd) {
                border-bottom:1px solid #ddd;
            }
            table tr td h5 {
              text-align: center;
            }
            table tr td h4 {
              text-align: center;
            }
            .pad-left-right-space{
                border: unset !important;
            }
            .pad-left-right-space td{
                padding: 5px 15px;
            }
            .pad-left-right-space td p{
                margin: 0;
            }
            .pad-left-right-space td b{
                font-size:15px;
                font-family: 'Roboto', sans-serif;
            }
            .order-detail th{
                font-size:16px;
                padding:15px;
                text-align:center;
                background: #fafafa;
            }
            .footer-social-icon tr td img{
                margin-left:5px;
                margin-right:5px;
            }
            #total {
                font-family: 'Roboto', sans-serif;
            }
            #subtotal {
                font-family: 'Roboto', sans-serif;
            }
        </style>
    </head>
    <body style="margin: 20px auto;">
    <?php
        $customer = $data['customer'];
        $detail = $data['details'];
        $total = 0;
    ?>
        <table align="center" border="0" cellpadding="0" cellspacing="0" style="padding: 0 30px;background-color: #fff; -webkit-box-shadow: 0px 0px 14px -4px rgba(0, 0, 0, 0.2705882353);box-shadow: 0px 0px 14px -4px rgba(0, 0, 0, 0.2705882353);width: 100%;">
            <tbody>
                <tr>
                    <td>
                        <table align="left" border="0" cellpadding="0" cellspacing="0" style="text-align: left;" width="100%">
                            <tr>
                                <td style="text-align: center;">
                                    <img src="https://shop.tecnimueble.com.ec/assets/images/icon/logo/1.png" alt="" style=";margin-bottom: 30px;max-width: 300px">
                                </td>
                            </tr>                            
                           <tr>
                                <td>
                                    <p style="font-size: 14px;"><b>Hola, <span id="userCompleteName"><?php echo $customer['name'].' '.$customer['lastname']; ?></span></b></p>
                                    <p style="font-size: 14px;">Tu orden ha sido confirmada</p>
                                    <p style="font-size: 14px;">Id de orden: <span id="orderId"><?php echo $data['id']; ?></span></p>
                                </td>
                            </tr> 
                        </table>
                       
                        <table cellpadding="0" cellspacing="0" border="0" align="left" style="width: 100%;margin-top: 10px;    margin-bottom: 10px;">
                                <tbody>
                                    <tr>
                                    <td style="background-color: #fafafa;border: 1px solid #ddd;padding: 15px;letter-spacing: 0.3px;width: 50%;">
                                        <h5 style="font-size: 16px; font-weight: 600;color: #000; line-height: 16px; padding-bottom: 13px; border-bottom: 1px solid #e6e8eb; letter-spacing: -0.65px; margin-top:0; margin-bottom: 13px;">Datos del cliente</h5>
                                        <p style="text-align: left;font-weight: normal; font-size: 14px; color: #000000;line-height: 21px;    margin-top: 0;">
                                          <strong>Número de identificación:</strong>&nbsp;&nbsp;<span id="id_num"><?php echo $customer['id_num']; ?></span>
                                          <br />
                                          <strong>Número de Telefono:</strong>&nbsp;&nbsp;<span id="phone"><?php echo $customer['phone']; ?></span>
                                          <br />
                                          <strong>Dirección:</strong>&nbsp;&nbsp;<span id="address"><?php echo $customer['addressLine1'].' '.$customer['addressLine2']; ?></span>
                                          <br />
                                          <strong>Ciudad:</strong>&nbsp;&nbsp;<span id="city"><?php echo $customer['city']; ?></span>
                                          <br />
                                          <strong>País:</strong>&nbsp;&nbsp;<span id="country"><?php echo $customer['country']; ?></span>
                                        </p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="order-detail" border="0" cellpadding="0" cellspacing="0"  align="left" style="width: 100%;    margin-bottom: 50px;">
                            <tr align="left" id="tableHeader">
                                <th>IMAGEN</th>
                                <th>PRODUCTO</th>
                                <th style="padding-left: 15px;">DESCRIPCIÓN</th>
                                <th>CANTIDAD</th>
                                <th>PRECIO</th>
                            </tr>
                            <?php
                                $tableFirstId = 'tableHeader';
                                for ($i = 0; $i < count($detail); $i += 1) {
                                    $obj = $detail[$i];
                                    $value = $obj['price'] * $obj['quantity'];
                                    $total += $value;
                                    $product = $detail[$i];
                                    $productData = $product['product'];
                                    $image = 'https://admin.tecnimueble.com.ec/media/'.$productData['media'][0]['id'].'/'.$productData['media'][0]['file_name'];
                                    $stringRow = '<tr id="tr_'.$i.'">'.
                                                '<td><h5><a href="'.$image.'" target="_blank"><img src="'.$image.'" alt="product" style="width: 150px;" /></a></h5></td>'.
                                                '<td><h5>'.$productData['name'].'</h5></td>'.
                                                '<td><h5>'.$productData['description'].'</h5></td>'.
                                                '<td><h4>'.$product['quantity'].'</h4></td>'.
                                                '<td><h4>$'.$product['price'].'</h4></td></tr>';
                                    echo $stringRow;
                                }
                            ?>
                            <tr class="pad-left-right-space ">
                                <td class="m-t-5" colspan="3" align="left">
                                    <p style="font-size: 18px;">Subtotal : </p>
                                </td>
                                <td class="m-t-5" colspan="2" align="right">
                                    <b>$<span id="subtotal" style="font-size: 18px;" ><?php echo $total; ?></span></b>
                                </td>
                            <tr class="pad-left-right-space ">
                                <td class="m-b-5" colspan="3" align="left">
                                    <p style="font-size: 18px;">Total :</p>
                                </td>
                                <td class="m-b-5" colspan="2" align="right">
                                    <b>$<span id="total" style="font-size: 18px;"><?php echo $total; ?></span></b>
                                </td>
                            </tr>
                           
                        </table>
                        
                    </td>
                </tr>
            </tbody>            
        </table>
    </body>
</html>