import AppListing from '../app-components/Listing/AppListing';

Vue.component('payment-listing', {
    mixins: [AppListing],
    data() {
        return {
            showCustomersFilter: false,
            customersMultiselect: {},
    
            filters: {
                customers: [],
            },
        }
    },
    watch: {
        showCustomersFilter: function (newVal, oldVal) {
            this.customersMultiselect = [];
        },
        customersMultiselect: function(newVal, oldVal) {
            this.filters.customers = newVal.map(function(object) { return object['key']; });
            this.filter('customers', this.filters.customers);
        }
    }
});