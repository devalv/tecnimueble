import AppForm from '../app-components/Form/AppForm';

Vue.component('payment-form', {
    mixins: [AppForm],
    props: ['customers'],
    data: function() {
        return {
            form: {
                customer:  '',
                cardNumber:  '' ,
                cardMonth:  '' ,
                cardYear:  '' ,
                cardCVC:  '' ,
                amount:  '' ,
                paymentDate:  '' ,
                
            }
        }
    }

});