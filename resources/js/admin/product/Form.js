import AppForm from '../app-components/Form/AppForm';

Vue.component('product-form', {
    mixins: [AppForm],
    props: [
        'categories'
    ],
    data: function() {
        return {
            form: {
                code:  '' ,
                name:  '' ,
                price:  '' ,
                cost:  '' ,
                category:  '' ,
                description:  '' ,
                stock:  '' ,
                origin:  '',
                is_featured: false
            },
            mediaCollections: ['images']
        }
    }

});