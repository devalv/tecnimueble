import AppForm from '../app-components/Form/AppForm';

Vue.component('order-form', {
    mixins: [AppForm],
    props: [
        'customers',
        'order_states'
    ],
    data: function() {
        return {
            form: {
                order_date:  '' ,
                shipped_date:  '' ,
                customer:  '' ,
                order_state:  '' ,
                notes:  '' 
            }
        }
    }
});