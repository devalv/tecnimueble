import AppListing from '../app-components/Listing/AppListing';

Vue.component('orders-listing', {
    mixins: [AppListing],
    data() {
        return {
            showCustomersFilter: false,
            customersMultiselect: {},
            showOrderStatesFilter: false,
            orderStatesMultiselect: {},
            filters: {
                customers: [],
                orderStates: []
            }
        }
    },
    watch: {
        showCustomersFilter: function (newVal, oldVal) {
            this.customersMultiselect = [];
        },
        showOrderStatesFilter: function (newVal, oldVal) {
            this.orderStatesMultiselect = [];
        },
        customersMultiselect: function(newVal, oldVal) {
            this.filters.customers = newVal.map(function(object) { 
                return object['key']; 
            });
            this.filter('customers', this.filters.customers);
        },
        orderStatesMultiselect: function(newVal, oldVal) {
            this.filters.orderStates = newVal.map(function(object) { 
                return object['key']; 
            });
            this.filter('orderStates', this.filters.orderStates);
        }
    }
});