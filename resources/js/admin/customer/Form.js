import AppForm from '../app-components/Form/AppForm';

Vue.component('customer-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                id_num:  '' ,
                id_type:  '' ,
                name:  '' ,
                lastname:  '' ,
                email:  '' ,
                password:  '' ,
                enabled:  false ,
                phone:  '' ,
                addressLine1:  '' ,
                addressLine2:  '' ,
                city:  '' ,
                country:  '' ,
                
            }
        }
    }

});