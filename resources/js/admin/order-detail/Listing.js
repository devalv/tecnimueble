import AppListing from '../app-components/Listing/AppListing';

Vue.component('order-detail-listing', {
    mixins: [AppListing],
    data() {
        return {
            showProductsFilter: false,
            productsMultiselect: {},
            showOrdersFilter: false,
            ordersMultiselect: {},
            filters: {
                products: [],
                orders: []
            }
        }
    },
    watch: {
        showProductsFilter: function (newVal, oldVal) {
            this.productsMultiselect = [];
        },
        showOrdersFilter: function (newVal, oldVal) {
            this.ordersMultiselect = [];
        },
        productsMultiselect: function(newVal, oldVal) {
            this.filters.products = newVal.map(function(object) { 
                return object['key']; 
            });
            this.filter('products', this.filters.products);
        },
        ordersMultiselect: function(newVal, oldVal) {
            this.filters.orders = newVal.map(function(object) { 
                return object['key']; 
            });
            this.filter('orders', this.filters.orders);
        }
    }
});