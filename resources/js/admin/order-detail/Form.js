import AppForm from '../app-components/Form/AppForm';

Vue.component('order-detail-form', {
    mixins: [AppForm],
    props: [
        'orders',
        'products'
    ],
    data: function() {
        return {
            form: {
                order:  '' ,
                product:  '' ,
                quantity:  '' ,
                price:  '' ,
                
            }
        }
    }

});