import AppListing from '../app-components/Listing/AppListing';

Vue.component('order-state-listing', {
    mixins: [AppListing]
});