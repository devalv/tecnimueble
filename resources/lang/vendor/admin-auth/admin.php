<?php

return [
    'auth_global' => [
        'email' => 'Correo electrónico',
        'password' => 'Contraseña',
        'password_confirm' => 'Confirmar contraseña',
    ],

    'login' => [
        'title' => 'Iniciar Sesión',
        'sign_in_text' => 'Inicia sesión en tu cuenta',
        'button' => 'Iniciar sesión',
        'forgot_password' => 'Olvidé mi contraseña',
    ],

    'password_reset' => [
        'title' => 'Resetear contraseña',
        'note' => 'Resetear contraseña',
        'button' => 'Resetear contraseña',
    ],

    'forgot_password' => [
        'title' => 'Resetear Contraseña',
        'note' => 'Enviar correo de recuperación',
        'button' => 'Enviar enlace de recuperación',
    ],

    'activation_form' => [
        'title' => 'Activar cuenta',
        'note' => 'Enviar enlace de activación',
        'button' => 'Enviar enlace de activación',
    ],

    'activations' => [
        'sent' => 'We have sent you an activation link!',
        'activated' => 'Your account was activated!',
        'invalid_request' => 'The request failed.',
        'disabled' => 'Activation is disabled.',
    ],

    'passwords' => [
        'reset' => 'Se ha reiniciado su contraseña',
        'sent' => 'Hemos enviado una contraseña nueva',
        'invalid_password' => 'Password must be at least six characters long and match the confirmation.',
        'invalid_token' => 'The password reset token is invalid.',
        'invalid_user' => "We can't find a user with this e-mail address.",
    ]
];
