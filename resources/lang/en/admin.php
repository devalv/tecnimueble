<?php

return [
    'admin-user' => [
        'title' => 'Users',

        'actions' => [
            'index' => 'Users',
            'create' => 'New User',
            'edit' => 'Edit :name',
            'edit_profile' => 'Edit Profile',
            'edit_password' => 'Edit Password',
        ],

        'columns' => [
            'id' => 'ID',
            'last_login_at' => 'Last login',
            'activated' => 'Activated',
            'email' => 'Email',
            'first_name' => 'First name',
            'forbidden' => 'Forbidden',
            'language' => 'Language',
            'last_name' => 'Last name',
            'password' => 'Password',
            'password_repeat' => 'Password Confirmation',
                
            //Belongs to many relations
            'roles' => 'Roles',
                
        ],
    ],

    'post' => [
        'title' => 'Posts',

        'actions' => [
            'index' => 'Posts',
            'create' => 'New Post',
            'edit' => 'Edit :name',
            'will_be_published' => 'Post will be published at',
        ],

        'columns' => [
            'id' => 'ID',
            'title' => 'Title',
            'slug' => 'Slug',
            'perex' => 'Perex',
            'published_at' => 'Published at',
            'enabled' => 'Enabled',
            
        ],
    ],

    'client' => [
        'title' => 'Clients',

        'actions' => [
            'index' => 'Clients',
            'create' => 'New Client',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'id_num' => 'Id num',
            'id_type' => 'Id type',
            'name' => 'Name',
            'lastname' => 'Lastname',
            'email' => 'Email',
            'enabled' => 'Enabled',
            'phone' => 'Phone',
            'addressLine1' => 'AddressLine1',
            'addressLine2' => 'AddressLine2',
            'city' => 'City',
            'country' => 'Country',
            
        ],
    ],

    'customer' => [
        'title' => 'Customers',

        'actions' => [
            'index' => 'Customers',
            'create' => 'New Customer',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'id_num' => 'Id num',
            'id_type' => 'Id type',
            'name' => 'Name',
            'lastname' => 'Lastname',
            'email' => 'Email',
            'password' => 'Password',
            'enabled' => 'Enabled',
            'phone' => 'Phone',
            'addressLine1' => 'AddressLine1',
            'addressLine2' => 'AddressLine2',
            'city' => 'City',
            'country' => 'Country',
            
        ],
    ],

    'product' => [
        'title' => 'Products',

        'actions' => [
            'index' => 'Products',
            'create' => 'New Product',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'code' => 'Code',
            'name' => 'Name',
            'price' => 'Price',
            'cost' => 'Cost',
            'category' => 'Category',
            'description' => 'Description',
            'stock' => 'Stock',
            'origin' => 'Origin',
            
        ],
    ],

    'category' => [
        'title' => 'Categories',

        'actions' => [
            'index' => 'Categories',
            'create' => 'New Category',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'image' => 'Image',
            
        ],
    ],

    'product' => [
        'title' => 'Products',

        'actions' => [
            'index' => 'Products',
            'create' => 'New Product',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'code' => 'Code',
            'name' => 'Name',
            'price' => 'Price',
            'cost' => 'Cost',
            'category_id' => 'Category',
            'description' => 'Description',
            'stock' => 'Stock',
            'origin' => 'Origin',
            'image' => 'Image',
            
        ],
    ],

    'order-state' => [
        'title' => 'Order States',

        'actions' => [
            'index' => 'Order States',
            'create' => 'New Order State',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'name' => 'Name',
            
        ],
    ],

    'order' => [
        'title' => 'Orders',

        'actions' => [
            'index' => 'Orders',
            'create' => 'New Order',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'order_date' => 'Order date',
            'shipped_date' => 'Shipped date',
            'status_id' => 'Status',
            'customer_id' => 'Customer',
            'notes' => 'Notes',
            
        ],
    ],

    'order-detail' => [
        'title' => 'Order Details',

        'actions' => [
            'index' => 'Order Details',
            'create' => 'New Order Detail',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'order_id' => 'Order',
            'product_id' => 'Product',
            'quantity' => 'Quantity',
            'price' => 'Price',
            
        ],
    ],

    'payment' => [
        'title' => 'Payments',

        'actions' => [
            'index' => 'Payments',
            'create' => 'New Payment',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'customer_id' => 'Customer',
            'cardNumber' => 'CardNumber',
            'cardMonth' => 'CardMonth',
            'cardYear' => 'CardYear',
            'cardCVC' => 'CardCVC',
            'amount' => 'Amount',
            'paymentDate' => 'PaymentDate',
            
        ],
    ],

    // Do not delete me :) I'm used for auto-generation
];