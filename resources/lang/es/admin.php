<?php

return [
    'admin-user' => [
        'title' => 'Usuarios',

        'actions' => [
            'index' => 'Usuarios',
            'create' => 'Nuevo Usuario',
            'edit' => 'Editar :name',
            'edit_profile' => 'Editar Perfil',
            'edit_password' => 'Editar Contraseña',
        ],

        'columns' => [
            'id' => 'ID',
            'last_login_at' => 'Último inicio de sesión',
            'activated' => 'Activado',
            'email' => 'Email',
            'first_name' => 'Primer nombre',
            'forbidden' => 'Prohibido',
            'language' => 'Idioma',
            'last_name' => 'Apellido',
            'password' => 'Contraseña',
            'password_repeat' => 'Confirmar contraseña',
                
            //Belongs to many relations
            'roles' => 'Roles',
                
        ],
    ],

    // Do not delete me :) I'm used for auto-generation
];