<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Se ha reseteado tu contraseña!',
    'sent' => 'Te hemos enviado un correo con un link para reiniciar tu contraseña!',
    'throttled' => 'Por favor espera antes de volver a intentar.',
    'token' => 'El token de reinicio de contraseña es incorrecto.',
    'user' => "No podemos encontrar ese correo electrónico en nuestra base de datos.",

];
