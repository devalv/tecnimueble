<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Mail;
use App\Mail\NewOrder;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/email', function () {
  $demo = [
    "headers" => [], 
    "original" => [
      "id" => 46, 
      "order_date" => "2021-04-07T00:00:00.000000Z", 
      "shipped_date" => null, 
      "status_id" => 1, 
      "customer_id" => 1, 
      "notes" => "Estas son notas", 
      "created_at" => "2021-04-07T06:06:40.000000Z", 
      "updated_at" => "2021-04-07T06:06:40.000000Z", 
      "details" => [
        [
          "id" => 62, 
          "order_id" => 46, 
          "product_id" => 3, 
          "quantity" => 2, 
          "price" => 100, 
          "created_at" => "2021-04-07T06:06:41.000000Z", 
          "updated_at" => "2021-04-07T06:06:41.000000Z", 
          "product" => [
              "id" => 3, 
              "code" => "COM-01", 
              "name" => "Comedor", 
              "price" => 100, 
              "cost" => 90, 
              "category_id" => 4, 
              "description" => "test", 
              "stock" => 186, 
              "origin" => "Quito", 
              "created_at" => "2021-03-21T18:13:43.000000Z", 
              "updated_at" => "2021-04-07T18:06:41.000000Z", 
              "is_featured" => 1, 
              "media" => [
                [
                    "id" => 22, 
                    "model_type" => "App\Models\Product", 
                    "model_id" => 3, 
                    "uuid" => "7c24ce0f-491a-4869-a3c0-6d7879ceb2bd", 
                    "collection_name" => "images", 
                    "name" => "g1WOfx8JlgoRGBqT2fEmfDCyhfw7j3mjsuZcwWZ4", 
                    "file_name" => "g1WOfx8JlgoRGBqT2fEmfDCyhfw7j3mjsuZcwWZ4.webp", 
                    "mime_type" => "image/webp", 
                    "disk" => "media", 
                    "conversions_disk" => "media", 
                    "size" => 270770, 
                    "manipulations" => [
                    ], 
                    "custom_properties" => [
                      "name" => "comedor.webp", 
                      "file_name" => "comedor.webp", 
                      "width" => 1400, 
                      "height" => 1400, 
                      "generated_conversions" => [
                        "thumb_200" => true 
                      ] 
                      ], 
                    "responsive_images" => [], 
                    "order_column" => 17, 
                    "created_at" => "2021-04-03T10:08:50.000000Z", 
                    "updated_at" => "2021-04-03T10:08:53.000000Z" 
                ] 
                     ], 
                     "resource_url" => "http://localhost:8000/admin/products/3", 
                     "category" => [
                                          "id" => 4, 
                                          "name" => "Comedor", 
                                          "description" => "Comedor", 
                                          "created_at" => "2021-03-19T14:54:24.000000Z", 
                                          "updated_at" => "2021-03-19T14:54:24.000000Z", 
                                          "resource_url" => "http://localhost:8000/admin/categories/4" 
                                       ] 
                  ], 
                  "resource_url" => "http://localhost:8000/admin/order-details/62" 
               ], 
               [
                                             "id" => 63, 
                                             "order_id" => 46, 
                                             "product_id" => 5, 
                                             "quantity" => 2, 
                                             "price" => 400, 
                                             "created_at" => "2021-04-07T06:06:42.000000Z", 
                                             "updated_at" => "2021-04-07T06:06:42.000000Z", 
                                             "product" => [
                                                "id" => 5, 
                                                "code" => "Co-01", 
                                                "name" => "Cocina Modular", 
                                                "price" => 400, 
                                                "cost" => 120, 
                                                "category_id" => 1, 
                                                "description" => "Este es un mueble de cocina modular", 
                                                "stock" => 186, 
                                                "origin" => "Quito", 
                                                "created_at" => "2021-04-03T10:07:48.000000Z", 
                                                "updated_at" => "2021-04-07T18:06:42.000000Z", 
                                                "is_featured" => 1, 
                                                "media" => [
                                                   [
                                                      "id" => 21, 
                                                      "model_type" => "App\Models\Product", 
                                                      "model_id" => 5, 
                                                      "uuid" => "e4caa064-7d32-40bb-af5c-11550598369b", 
                                                      "collection_name" => "images", 
                                                      "name" => "hGcXYOd1Hpzz1xBU4SF0pQffhrjGZc9Fgm7n3ivJ", 
                                                      "file_name" => "hGcXYOd1Hpzz1xBU4SF0pQffhrjGZc9Fgm7n3ivJ.webp", 
                                                      "mime_type" => "image/webp", 
                                                      "disk" => "media", 
                                                      "conversions_disk" => "media", 
                                                      "size" => 89262, 
                                                      "manipulations" => [
                                                      ], 
                                                      "custom_properties" => [
                                                            "name" => "cocina.webp", 
                                                            "file_name" => "cocina.webp", 
                                                            "width" => 1400, 
                                                            "height" => 1400, 
                                                            "generated_conversions" => [
                                                               "thumb_200" => true 
                                                            ] 
                                                         ], 
                                                      "responsive_images" => [
                                                               ], 
                                                      "order_column" => 16, 
                                                      "created_at" => "2021-04-03T10:07:49.000000Z", 
                                                      "updated_at" => "2021-04-03T10:07:52.000000Z" 
                                                   ], 
                                                   [
                                                                     "id" => 30, 
                                                                     "model_type" => "App\Models\Product", 
                                                                     "model_id" => 5, 
                                                                     "uuid" => "7f9fb58b-d34e-4058-b0b3-6c9ed13f5613", 
                                                                     "collection_name" => "images", 
                                                                     "name" => "eCsXwerOH4q2pQk1be2mP9kHzgyUCKVlVItoHuV0", 
                                                                     "file_name" => "eCsXwerOH4q2pQk1be2mP9kHzgyUCKVlVItoHuV0.webp", 
                                                                     "mime_type" => "image/webp", 
                                                                     "disk" => "media", 
                                                                     "conversions_disk" => "media", 
                                                                     "size" => 26214, 
                                                                     "manipulations" => [
                                                                     ], 
                                                                     "custom_properties" => [
                                                                           "name" => "modularcocina.webp", 
                                                                           "file_name" => "modularcocina.webp", 
                                                                           "width" => 700, 
                                                                           "height" => 394, 
                                                                           "generated_conversions" => [
                                                                              "thumb_200" => true 
                                                                           ] 
                                                                        ], 
                                                                     "responsive_images" => [
                                                                              ], 
                                                                     "order_column" => 25, 
                                                                     "created_at" => "2021-04-03T15:31:13.000000Z", 
                                                                     "updated_at" => "2021-04-03T15:31:14.000000Z" 
                                                                  ] 
                                                ], 
                                                "resource_url" => "http://localhost:8000/admin/products/5", 
                                                "category" => [
                                                                                    "id" => 1, 
                                                                                    "name" => "Cocina", 
                                                                                    "description" => "Muebles de cocina", 
                                                                                    "created_at" => "2021-01-06T03:06:13.000000Z", 
                                                                                    "updated_at" => "2021-01-06T03:06:13.000000Z", 
                                                                                    "resource_url" => "http://localhost:8000/admin/categories/1" 
                                                                                 ] 
                                             ], 
                                             "resource_url" => "http://localhost:8000/admin/order-details/63" 
                                          ] 
            ], 
        "resource_url" => "http://localhost:8000/admin/orders/46", 
        "order_state" => [
          "id" => 1, 
          "name" => "Generada", 
          "created_at" => "2021-01-06T06:43:43.000000Z", 
          "updated_at" => "2021-01-06T06:43:43.000000Z", 
          "resource_url" => "http://localhost:8000/admin/order-states/1" 
        ], 
        "customer" => [
          "id" => 1, 
          "id_num" => "1718819661", 
          "id_type" => "Cédula", 
          "name" => "Bryan", 
          "lastname" => "Alvarado", 
          "email" => "bryalv77@hotmail.com", 
          "enabled" => 1, 
          "phone" => "0987767896", 
          "addressLine1" => "Quito", 
          "addressLine2" => "Norte", 
          "city" => "Quito", 
          "country" => "Ecuador", 
          "created_at" => "2021-01-07T07:50:12.000000Z", 
          "updated_at" => "2021-04-04T18:22:52.000000Z", 
          "cart" => null, 
          "resource_url" => "http://localhost:8000/admin/customers/1" 
        ] 
      ], 
      "exception" => null 
    ];  
    Mail::to('bryalv77@hotmail.com')->send(new NewOrder($demo));
    return new NewOrder($demo);
});


// Customers
Route::get('customers', 'App\Http\Controllers\Admin\CustomersController@getAllCustomers');
Route::get('customers/id/{id}', 'App\Http\Controllers\Admin\CustomersController@getCustomerById');
Route::post('customers/login', 'App\Http\Controllers\Admin\CustomersController@login');
Route::post('customers/register', 'App\Http\Controllers\Admin\CustomersController@register');
Route::post('customers/{id}/updateCart', 'App\Http\Controllers\Admin\CustomersController@updateCart');

// Categories
Route::get('categories', 'App\Http\Controllers\Admin\CategoriesController@getAllCategories');
Route::get('categories/{id}', 'App\Http\Controllers\Admin\CategoriesController@getCategoryById');

//Products
Route::get('products', 'App\Http\Controllers\Admin\ProductsController@getAllProducts');
Route::post('getProducts', 'App\Http\Controllers\Admin\ProductsController@getProducts');
Route::get('products/featured', 'App\Http\Controllers\Admin\ProductsController@getFeaturedProducts');
Route::get('products/{id}', 'App\Http\Controllers\Admin\ProductsController@getProductById');
Route::get('products/category/{id}', 'App\Http\Controllers\Admin\ProductsController@getProductsByCategory');

//Orders
Route::get('orders', 'App\Http\Controllers\Admin\OrdersController@getAllOrders');
Route::get('orders/{id}', 'App\Http\Controllers\Admin\OrdersController@getOrderById');
Route::get('orders/customer/{id}', 'App\Http\Controllers\Admin\OrdersController@getOrderByCustomerId');
Route::post('orders/{customer_id}', 'App\Http\Controllers\Admin\OrdersController@placeOrder');
