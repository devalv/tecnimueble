<?php

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Brackets\AdminAuth\Models\AdminUser::class, function (Faker\Generator $faker) {
    return [
        'activated' => true,
        'created_at' => $faker->dateTime,
        'deleted_at' => null,
        'email' => $faker->email,
        'first_name' => $faker->firstName,
        'forbidden' => $faker->boolean(),
        'language' => 'es',
        'last_login_at' => $faker->dateTime,
        'last_name' => $faker->lastName,
        'password' => bcrypt($faker->password),
        'remember_token' => null,
        'updated_at' => $faker->dateTime,
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Customer::class, static function (Faker\Generator $faker) {
    return [
        'id_num' => $faker->sentence,
        'id_type' => $faker->sentence,
        'name' => $faker->firstName,
        'lastname' => $faker->sentence,
        'email' => $faker->email,
        'password' => bcrypt($faker->password),
        'enabled' => $faker->boolean(),
        'phone' => $faker->sentence,
        'addressLine1' => $faker->sentence,
        'addressLine2' => $faker->sentence,
        'city' => $faker->sentence,
        'country' => $faker->sentence,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Category::class, static function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'description' => $faker->sentence,
        'image' => $faker->sentence,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Product::class, static function (Faker\Generator $faker) {
    return [
        'code' => $faker->sentence,
        'name' => $faker->firstName,
        'price' => $faker->randomFloat,
        'cost' => $faker->randomFloat,
        'category_id' => $faker->randomNumber(5),
        'description' => $faker->sentence,
        'stock' => $faker->randomNumber(5),
        'origin' => $faker->sentence,
        'image' => $faker->sentence,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\OrderState::class, static function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Order::class, static function (Faker\Generator $faker) {
    return [
        'order_date' => $faker->date(),
        'shipped_date' => $faker->date(),
        'status_id' => $faker->randomNumber(5),
        'customer_id' => $faker->randomNumber(5),
        'notes' => $faker->sentence,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\OrderDetail::class, static function (Faker\Generator $faker) {
    return [
        'order_id' => $faker->randomNumber(5),
        'product_id' => $faker->randomNumber(5),
        'quantity' => $faker->randomNumber(5),
        'price' => $faker->randomFloat,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Payment::class, static function (Faker\Generator $faker) {
    return [
        'customer_id' => $faker->randomNumber(5),
        'cardNumber' => $faker->sentence,
        'cardMonth' => $faker->sentence,
        'cardYear' => $faker->sentence,
        'cardCVC' => $faker->sentence,
        'amount' => $faker->randomFloat,
        'paymentDate' => $faker->date(),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});
